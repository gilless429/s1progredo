from math import sqrt
import re
from collections import defaultdict
import heapq
import matplotlib.pyplot as plt
from copy import deepcopy


def moyenne(tab):
    return sum(tab) / len(tab)


def ecartype(tab):
    return sqrt((sum([x ** 2 for x in tab]) - moyenne(tab) ** 2 * len(tab)) / (len(tab) - 1))


with open('fragments1000.faa', 'r') as f1:
    countSeq = 0
    ATCGonly = True
    seqslength = []
    ATper = []
    for li in f1:
        ATCount = 0
        if li.startswith(">"):
            countSeq += 1
        else:
            seqslength.append(len(li.strip()))
            if re.search("[^ATCG]", li):
                ATCGonly = False
            for nuc in li:
                if nuc.upper() == "A" or nuc.upper() == "T":
                    ATCount += 1
            ATper.append(ATCount / len(li.strip()) * 100)

with open('resultat_Blast1000.tab', 'r') as f2:
    BLASTres_nb = BLASTres_Signb = 0
    BLASTres_Sigscores = []
    Frag_resSig = set()
    Clone_resSig = set()
    LineScore = defaultdict(float)
    taxCodeSig = defaultdict(int)
    geneCodeSig = defaultdict(int)
    taxCodeBestRes = defaultdict(int)
    BestResFragTaxCode = defaultdict(set)
    FragsBestObt = set()
    for li in f2:
        BLASTres_nb += 1
        if float(li.split('\t')[10]) < 10 ** -5 and float(li.split()[11]) > 50:
            BLASTres_Signb += 1
            BLASTres_Sigscores.append(float(li.split()[11]))
            Frag_resSig.add(li.split()[0])
            Clone_resSig.add(li.split()[0].split('_')[0])
            LineScore[li] = float(li.split()[11])
            taxCodeSig[li.split()[1].split(":")[0]] += 1
            geneCodeSig[li.split()[1]] += 1
            if li.split()[0] not in FragsBestObt:
                taxCodeBestRes[li.split()[1].split(":")[0]] += 1
                BestResFragTaxCode[li.split()[0].split('_')[0]].add(li.split()[1].split(":")[0])
                FragsBestObt.add(li.split()[0])

with open('taxonomy', 'r') as f3:
    actinoLines = 0
    current3hash = ""
    threeHnbdic = defaultdict(int)
    threeHnbSigResdic = defaultdict(int)
    threeHnbBestResdic = defaultdict(int)
    ActinoCode = set()
    for li in f3:
        if li.startswith('### '):
            current3hash = "".join(li.strip().split()[1:])
            threeHnbdic[current3hash] = 0
        elif not (li.startswith("#")) and re.search('[a-zA-Z0-9]', li):
            threeHnbdic[current3hash] += 1
            threeHnbSigResdic[current3hash] += taxCodeSig[li.split()[1]]
            threeHnbBestResdic[current3hash] += taxCodeBestRes[li.split()[1]]
            if current3hash == "Actinobacteria":
                actinoLines += 1
                ActinoCode.add(li.split()[1])

geneCodeSigActino = deepcopy(geneCodeSig)
for gene in geneCodeSigActino:
    if gene.split(':')[0] not in ActinoCode:
        geneCodeSigActino[gene] = 0

with open('annotation_genes', 'r') as f5:
    nbGeneWithKO = nbGeneWithPATH = nbGeneWithKOPATH = 0
    nbPerPath = defaultdict(int)
    KOSig = defaultdict(int)
    PATHSig = defaultdict(int)
    PATHSigActino = defaultdict(int)
    for li in f5:
        if re.search('K[0-9]{5}', li):
            nbGeneWithKO += 1
            if li.split()[0] in geneCodeSig:
                for el in re.findall('K[0-9]{5}', li):
                    KOSig[el.strip()] += geneCodeSig[li.split()[0]]
            if re.search('ko[0-9]{5}', li):
                nbGeneWithKOPATH += 1
        if re.search('ko[0-9]{5}', li):
            nbGeneWithPATH += 1
            for el in re.findall('ko[0-9]{5}', li):
                nbPerPath[el] += 1
            if li.split()[0] in geneCodeSig:
                for el in re.findall('ko[0-9]{5}', li):
                    PATHSig[el.strip()] += geneCodeSig[li.split()[0]]
                if li.split()[0] in geneCodeSigActino:
                    for el in re.findall('ko[0-9]{5}', li):
                        PATHSigActino[el.strip()] += geneCodeSigActino[li.split()[0]]
maxPath = max(nbPerPath, key=nbPerPath.get)

with open('pathways', 'r') as f4:
    nbMetPaths = nbMetPathsXeno = nbMetPathsAmino = 0
    BcatXeno = CcatXeno = DcatXeno = BcatAmino = CcatAmino = DcatAmino = False
    AcatCount = defaultdict(int)
    for li in f4:
        if li.startswith('A') and re.search("[0-9]", li):
            currentAcat = li.split()[1].split('<')[0]
        elif li.startswith('B') and re.search("[0-9]", li):
            if "xenobiotic" in li.lower():
                BcatXeno = True
            else:
                BcatXeno = False
            if "amino acid" in li.lower() or "amino-acid" in li.lower():
                BcatAmino = True
            else:
                BcatAmino = False
        elif li.startswith("C") and "[PATH:ko" in li:
            if maxPath in li:
                NameMaxPath = re.search(r"([a-zA-Z ]+)\[", li).group(1).strip()
            nbMetPaths += 1
            DcatXeno = CcatXeno = DcatAmino = CcatAmino = False
            if re.findall('ko[0-9]{5}', li)[0] in PATHSig:
                AcatCount[currentAcat] += PATHSig[re.findall('ko[0-9]{5}', li)[0]]
            if BcatXeno:
                nbMetPathsXeno += 1
            elif "xenobiotic" in li.lower():
                CcatXeno = True
                nbMetPathsXeno += 1
            if BcatAmino:
                nbMetPathsAmino += 1
            elif "amino acid" in li.lower() or "amino-acid" in li.lower():
                CcatAmino = True
                nbMetPathsAmino += 1
        elif not BcatXeno and not CcatXeno and not DcatXeno and li.startswith('D'):
            if "xenobiotic" in li.lower():
                nbMetPathsXeno += 1
                DcatXeno = True
            elif "amino acid" in li.lower() or "amino-acid" in li.lower():
                nbMetPathsAmino += 1
                DcatAmino = True
AcatCountFreq = {key: value / BLASTres_Signb * 100 for key, value in AcatCount.items()}

print("\n_________________________Part I_________________________")
print("Number of sequences (fragments) :", countSeq)
print("Average length :", moyenne(seqslength), "Standard deviation length :", ecartype(seqslength))
if ATCGonly:
    print("ATCG only characters in sequences")
else:
    print("ATCG not only characters in sequences")
print("Average length :", moyenne(ATper), "%  Standard deviation length :", ecartype(ATper), "%")
print("\n_________________________Part II_________________________")
print("Number of results :", BLASTres_nb)
print("Number of significant results :", BLASTres_Signb)
print("Average score of significant results :", moyenne(BLASTres_Sigscores))
print("Number of fragments with at least one significant result :", len(Frag_resSig))
print("Number of clones with at least one significant result :", len(Clone_resSig))
print("5 results with best score :")
for res in heapq.nlargest(5, LineScore, key=LineScore.get):
    print(res.strip())
print("\n_________________________Part III_________________________")
print("Number of sequenced Actinobacteria found in KEGG :", actinoLines)
print("Clades (###) with more than 13 wholly sequenced genomes :")
for k, v in threeHnbdic.items():
    if v > 13:
        print(k, v)
plt.bar([k for k, v in threeHnbSigResdic.items() if v > 100], [v for k, v in threeHnbSigResdic.items() if v > 100])
# plt.show()
print("Number of best results for clades (###) with more than 25 best results :")
for k, v in threeHnbBestResdic.items():
    if v > 25:
        print(k, v)
print("Number of clones for which both fragments' best results are of the same clade (###) :",
      len([clone for clone in BestResFragTaxCode.keys() if len(BestResFragTaxCode[clone]) == 1]))
print("\n_________________________Part IV_________________________")
print("\n---- 1) ----")
print("Number of metabolic pathways (PATH) in KEGG :", nbMetPaths)
print("Number of metabolic pathways having something to do with xenobiotics :", nbMetPathsXeno)
print("Number of metabolic pathways having something to do with amino acids :", nbMetPathsAmino)
print("\n---- 2) ----")
print("Number of genes with at least one ortholog group (KO) associated :", nbGeneWithKO)
print("Number of genes with at least one pathway (ko) associated :", nbGeneWithPATH)
print("Average number of pathways an ortholog group is associated with :", nbGeneWithKOPATH / nbGeneWithKO)
print("\n---- 3) ----")
print("The name of the pathway most found in the annotation_genes file is :", NameMaxPath)
print("\n---- 4) ----")
print("5 orthologs most found in BLAST results :")
for KO in heapq.nlargest(5, KOSig, key=KOSig.get):
    print(KO, KOSig[KO])
print("5 pathways most found in BLAST results :")
for ko in heapq.nlargest(10, PATHSig, key=PATHSig.get):
    print(ko, PATHSig[ko] / BLASTres_Signb * 100, "%")
print("5 pathways most found in BLAST results corresponding to Actinobacteria :")
for ko in heapq.nlargest(10, PATHSigActino, key=PATHSigActino.get):
    print(ko, PATHSigActino[ko] / BLASTres_Signb * 100, "%")
print("Frequency of apparition of big functional categories in significant resuls of BLAST :")
for k, v in AcatCountFreq.items():
    print(k, v)
plt.clf()
plt.bar(AcatCountFreq.keys(), AcatCountFreq.values())
plt.show()
